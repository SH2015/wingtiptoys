using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WingTipToys.DL;
using WingTipToys.Services;
using WingTipToys.Services.Implementation;
using WingTipToys.Web.Web.Startup.AutoMapper;

namespace WingTipToys.Web.Host
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
               
                 .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
            var connectionString = Configuration.GetSection("ConnectionStrings").GetChildren().FirstOrDefault().Value;
            services.AddTransient<MsSqlProvider>(services => new MsSqlProvider(connectionString));
            services.AddSingleton<IProductProvider, ProductProvider>();
            services.AddSingleton<IProductService, ProductService>(); 
            var controllersAssembly = typeof(ProductController).Assembly;
            services.AddMvc();
            services.AddControllers().PartManager.ApplicationParts.Add(new AssemblyPart(controllersAssembly));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "WingTipToys", Version = "v1" });
               
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{controllersAssembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddAutoMapper(typeof(ProfileMapper).Assembly);
            services.AddCors(options =>
            {
                options.AddPolicy("allowall",
                builder =>
                {
                    builder.AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                    builder.SetIsOriginAllowed((string hostName) => true);
                });
            });
 
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

           

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("allowall");
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
           
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = String.Empty;
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "WingTipToys V1");
            });

        }
    }
}
