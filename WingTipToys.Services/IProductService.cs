﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WingTipToys.DL.DataModels;

namespace WingTipToys.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetProducts();
    }
}
