﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WingTipToys.DL.DataModels;

namespace WingTipToys.DL.Implementation
{
    public class ProductProvider : IProductProvider
    {
        private readonly MsSqlProvider msSqlProvider;

        public ProductProvider(MsSqlProvider msSqlProvider)
        {
            this.msSqlProvider = msSqlProvider;
        }

        public async Task<IEnumerable<Product>> GetByCategoryId(int categoryId)
        {
            //TODO: In line sql statement should never be used, but since we cant change the DB we cant add SP to do the job 
            using var command = msSqlProvider.GetSqlCommand("SELECT  [ProductID]      ,[ProductName]      ,[Description]      ,[ImagePath]      ,[UnitPrice]      ,[CategoryID]  FROM [dbo].[Products] where CategoryID = @categoryId", System.Data.CommandType.Text);
            command.Parameters.AddWithValue("@categoryId", categoryId);
            using var conn = command.Connection;
            conn.Open();
            using var dr = await command.ExecuteReaderAsync(CommandBehavior.CloseConnection);

            var products = new List<Product>();
            var ordinals = GetOrdinals(dr);
            while (await dr.ReadAsync())
            {
                products.Add(new Product
                {
                    ProductId = dr.GetInt32(ordinals["ProductID"]),
                    Name = dr.GetString(ordinals["ProductName"]), //TODO: We could add check for nulls for each one but for now we will assume no null values are expected
                    Description = dr.GetString(ordinals["Description"]),
                    CategoryID = dr.GetInt32(ordinals["CategoryID"]),
                    ImagePath = dr.GetString(ordinals["ImagePath"]),
                    UnitPrice = Convert.ToSingle(dr[ordinals["UnitPrice"]].ToString())

                }); ;
            }
            dr.Close();
            return products;
        }

        private static Dictionary<string, int> GetOrdinals(IDataReader dr)
        {
            var ordinals = new Dictionary<string, int>();
            ordinals.Add("ProductID", dr.GetOrdinal("ProductID"));
            ordinals.Add("ProductName", dr.GetOrdinal("ProductName"));
            ordinals.Add("Description", dr.GetOrdinal("Description"));
            ordinals.Add("ImagePath", dr.GetOrdinal("ImagePath"));
            ordinals.Add("UnitPrice", dr.GetOrdinal("UnitPrice"));
            ordinals.Add("CategoryID", dr.GetOrdinal("CategoryID"));
            return ordinals;

        }

    }
}
