﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WingTipToys.Web
{
    using AutoMapper;
    using WingTipToys.DL.DataModels;
    using WingTipToys.Web.Models;

    namespace Web.Startup.AutoMapper
    {
        public class ProfileMapper : Profile
        {
            public ProfileMapper()
            {
                CreateMap<Product, ProductModel>();
                CreateMap<ProductModel, Product>();
                
            }
        }
    }

}
