﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WingTipToys.Web.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public float UnitPrice { get; set; }
    }
}
