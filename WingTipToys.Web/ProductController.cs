﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WingTipToys.DL.DataModels;
using WingTipToys.Services;
using WingTipToys.Web.Models;
using System.Linq;

namespace WingTipToys.Web
{
    /// <summary>
    /// Product Methods 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController:ControllerBase
    {
        private readonly IProductService productService;
        private readonly IMapper mapper;

        public ProductController(IProductService productService, IMapper mapper)
        {
            this.productService = productService??throw new NullReferenceException(nameof(productService));
            this.mapper = mapper;
        }

        /// <summary>
        /// Get All products 
        /// </summary>
        /// <returns></returns>
        [Microsoft.AspNetCore.Mvc.HttpGet("getAll")]
        
        public async Task<IEnumerable<ProductModel>> GetProducts()
        {
            var products = await productService.GetProducts();
            return products.Select(x => mapper.Map<ProductModel>(x));
        }
    }
}
