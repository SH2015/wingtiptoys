﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WingTipToys.DL.DataModels;

namespace WingTipToys.DL
{
    public interface IProductProvider
    {
        Task<IEnumerable<Product>> GetByCategoryId(int categoryId);
    }
}