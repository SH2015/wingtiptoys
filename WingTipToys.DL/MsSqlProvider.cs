﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WingTipToys.DL
{
    public class MsSqlProvider
    {
        private readonly string connectionString;

        public MsSqlProvider(string connectionString)
        {
            this.connectionString = connectionString ?? throw new NullReferenceException(nameof(connectionString));
        }

        public SqlCommand GetSqlCommand (string commandText , CommandType commandType)
        {
            var conn = new SqlConnection(connectionString);
            var command = new SqlCommand(commandText, conn) { CommandType = commandType };
            return command;
        }
    }
}
