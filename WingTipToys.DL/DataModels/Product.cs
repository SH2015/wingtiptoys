﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WingTipToys.DL.DataModels
{
   public class Product
    {
        public int ProductId { get; set; }
        public string  Name { get; set; }
        public string  Description { get; set; }
        public string ImagePath { get; set; }
        public float UnitPrice { get; set; }
        public int CategoryID { get; set; }
    }
}
