﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WingTipToys.DL;
using WingTipToys.DL.DataModels;

namespace WingTipToys.Services.Implementation
{
    public class ProductService : IProductService
    {
        private readonly IProductProvider productProvider;
        private const int CarCategory = 1;
        public ProductService(IProductProvider productProvider)
        {
            this.productProvider = productProvider;
        }

        public async Task<IEnumerable<Product>> GetProducts() // Since business requirements doesnt require other categoies BL layer will not ask API for categoryID 
        {
            //TODO: Add caching to reduce pressure on database server 
            return await productProvider.GetByCategoryId(CarCategory);
        }
    }
}
