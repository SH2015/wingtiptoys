using Moq;
using System;
using WingTipToys.DL;
using WingTipToys.DL.DataModels;
using Xunit;
using System.Linq;
using System.Threading.Tasks;

namespace WingTipToys.Services.Implementation.Tests
{
    public class ProductServiceTests
    {
        private ProductService _testie; 
        [Fact(DisplayName = "Test Getting Product", Timeout =3000)]
        public async Task GetProductTest()
        {
            Mock<IProductProvider> productProviderMock = new Mock<IProductProvider>();
            var testProducts = new Product[] { new Product { ProductId = 1, CategoryID = 1, Description = "dec1", ImagePath = "path1", Name = "name2", UnitPrice = 55.6F } };
            productProviderMock.Setup(x => x.GetByCategoryId(It.IsAny<int>())).ReturnsAsync(testProducts);
            _testie = new ProductService(productProviderMock.Object);
            var returnedProducts = await _testie.GetProducts();
            Assert.True(testProducts.Length == returnedProducts.Count(),"Number of products did not match");
            foreach (var p in testProducts)
            {
                var rProduct = returnedProducts.First(x => x.ProductId == p.ProductId);
                Assert.Equal(p.Description, rProduct.Description);
                Assert.Equal(p.CategoryID, rProduct.CategoryID);
                Assert.Equal(p.ImagePath, rProduct.ImagePath);
                Assert.Equal(p.Name, rProduct.Name);
                Assert.Equal(p.UnitPrice, rProduct.UnitPrice);
            }
        }
    }
}
